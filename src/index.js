const express = require("express");
const layout = require("./layout");

const app = express();

app.use("/img", express.static(__dirname + "/img"));
app.use((req, res, next) => {
  console.log(`${new Date().toISOString()}: ${req.method} ${req.path}`);
  next();
});

app.get("/", (request, response) => {
  response.send(
    layout(`
    <p>Hello world!</p>
  `)
  );
});

const port = process.env.PORT || 8080;

app.listen(port, () => {
  console.log("Listening on port " + port);
});
