FROM node:13.1.0-alpine3.10

ENV NODE_ENV production
WORKDIR /app 

COPY package.json package-lock.json ./

RUN npm install

COPY src/ ./src

CMD npm run start
